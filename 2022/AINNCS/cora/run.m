function run()
    disp("Startup..")

    % add everything to path
    addpath(genpath("./cora"));

    % Check if everything is set up correctly:
    check = test_requiredToolboxes;
    if check
        disp('All Required Toolboxes are installed.')
    else
        disp("Make sure all required toolboxes are installed!")
        disp("See Manual: https://tumcps.github.io/CORA")
        return
    end

    mpt_init();

    % run benchmark
    run_arch22_ainncs_category();
end