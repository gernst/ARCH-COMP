function coeffs = getCoeffsByApproxType(approx_type, order, l, u, f, dfs)
% finds coefficients using the given approx_type
%
%
% Syntax:
%    coeffs = NNHelper.getCoeffsByApproxType(approx_type, order, l, u, f, dfs)
%
% Inputs:
%    approx_type - 'regression', 'ridgeregression', 'throw-catch'
%    order - polynomial order
%    l - lower bound
%    u - upper bound
%    f - function handle to approximate
%    dfs - cell array containing derivative function handles
%
% Outputs:
%    coeffs - polynomial coefficients
%             (decending order, e.g. contant term is at coeffs(end))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: respective coeffs calculation
%
% Author:       Tobias Ladner
% Written:      22-July-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

if nargin < 6
    error("wrong number of arguments")
end

% use 10 points within [l, u] for regression
x = linspace(l, u, 10);
y = f(x);

if strcmp(approx_type, "regression")
    % compute polynomial that best fits the activation function
    coeffs = NNHelper.leastSquarePolyFunc(x, y, order);
    coeffs = fliplr(coeffs'); % descending order

elseif strcmp(approx_type, "ridgeregression")
    % compute polynomial that best fits the activation function
    coeffs = NNHelper.leastSquareRidgePolyFunc(x, y, order);
    coeffs = fliplr(coeffs'); % descending order

elseif strcmp(approx_type, "throw-catch")
    if df(l) < df(u)
        coeffs = NNHelper.calcAlternatingDerCoeffs(l, u, order, f, dfs);
    else
        coeffs = NNHelper.calcAlternatingDerCoeffs(u, l, order, f, dfs);
    end
else
    error("Unknown approximation type '%s'", approx_type);
end
coeffs(isnan(coeffs)) = 0;

end