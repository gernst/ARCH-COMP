function hs = plus(summand1,summand2)
% plus - Overloaded '+' operator for the addition of a vector with a
%    halfspace
%
% Syntax:  
%    hs = plus(summand1,summand2)
%
% Inputs:
%    summand1 - halfspace object or numerical vector
%    summand2 - halfspace object or numerical vector
%
% Outputs:
%    hs - halfspace object
%
% Example: 
%    hs = halfspace([1 1],2);
%    summand = [1; -0.5];
%    hs + summand
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: mtimes

% Author:       Matthias Althoff, Mark Wetzlinger
% Written:      28-August-2013
% Last update:  16-March-2021 (MW, error handling)
% Last revision:---

%------------- BEGIN CODE --------------

% pre-processing: assign halfspace and summand
if isa(summand1,'halfspace')
    hs=summand1;
    summand=summand2;
    
elseif isa(summand2,'halfspace')
    % switch order
    hs=summand2;
    summand=summand1;  
end

% error handling
if isempty(hs)
    % empty case
    throw(CORAerror('CORA:emptySet'));
elseif ~isvector(summand)
    % summand not a vector
    throw(CORAerror('CORA:wrongValue','first or second',"numerical vector"));
elseif dim(hs) ~= length(summand)
    % dimension mismatch
    throw(CORAerror('CORA:dimensionMismatch','obj1',hs,'dim1',dim(hs),...
        'obj2',summand,'size2',size(summand)));
end


% compute Minkowski sum
if isnumeric(summand)
    hs.d = hs.d + hs.c.'*summand;
else
    throw(CORAerror('CORA:noops',hs,summand));
end

%------------- END OF CODE --------------