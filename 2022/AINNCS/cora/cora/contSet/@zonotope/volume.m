function vol = volume(Z,varargin)
% volume - computes the volume of a zonotope
%
% Syntax:  
%    vol = volume(Z,type,order)
%
% Inputs:
%    Z - zonotope object
%    type - (optional) type of approximation 
%           - 'none' (default)
%           - 'reduce' for reduced zonotope with order o
%           - 'alamo', see [2]
%    order - (optional) 
%
% Outputs:
%    vol - volume
%
% Example: 
%    Z = zonotope([1 -1 0; 0 0 -1]);
%    vol = volume(Z)
%
% References:
%    [1] E. Grover et al. "Determinants and the volumes of parallelotopes 
%        and zonotopes", 2010 
%    [2] Alamo et al. "Bounded error identification of systems with 
%        time-varying parameters", TAC 2006.
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      24-August-2007 
% Last update:  19-July-2010
%               02-September-2019 (incl. approx)
%               04-May-2020 (MW, add vol=0 cases)
%               09-September-2020 (MA, Alamo approx added, reduce changed)
%               11-July-2022 (ME, included batchCombinator)
% Last revision:---

%------------- BEGIN CODE --------------

% default value
type = 'exact';
order = 5;

if nargin >= 2
    if ~any(strcmp(varargin{1},{'exact','reduce','alamo'}))
        throw(CORAerror('CORA:wrongInput','second',"'exact', 'reduce', or 'alamo'"));
    end
    type = varargin{1};
end

if nargin >= 3
    if ~isnumeric(varargin{2}) || ~isscalar(varargin{2}) || varargin{2} < 1
        throw(CORAerror('CORA:wrongInput','third',...
            'Scalar integer value greater or equal to 1'));
    end
    order = varargin{2};
end

%dimension and nrOfGenerators
G=generators(Z);
[n,nrOfGens]=size(G);

if nrOfGens < n || rank(G) < n
    vol = 0;
    return
end

% exact computation
if strcmp(type,'exact')
    % exact calculation

    %possible combinations of n=dim generators from all generators
    combinator = batchCombinator(nrOfGens, int16(n));

    % number of combinations processed at once
    batch_size = min(combinator.BC, 1e7);

    accVol = 0;

    while combinator.done == false
        
        batch = combinator.nextBatch(batch_size);

        for j=1:length(batch(:,1))
            try
                currVol = abs(det(G(:,batch(j,:))));
                accVol = accVol + currVol;
            catch
                throw(CORAerror('CORA:specialError',...
                'Parallelotope volume could not be computed.'));            
            end
        end

    end

    % multiply result by factor
    vol=2^n*accVol;

% over-approximative volume using order reduction
elseif strcmp(type,'reduce')
    % reduce zonotope
    Zred = reduce(Z,'pca',order); 
    vol = volume(Zred);
  
% approximation according to [2]    
elseif strcmp(type,'alamo')
    vol = 2^n*sqrt(det(G*G')); 
end


%------------- END OF CODE --------------