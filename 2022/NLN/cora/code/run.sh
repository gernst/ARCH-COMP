#!/bin/bash
set -e

# reachable set computation with CORA
matlab -nodisplay -nosoftwareopengl -r \
  "main()"

# collision checks
echo "Collision Checker (TRAF22) ------------------------------------------------"
echo " "

for filename in /results/*occupancies.csv; do
    python3 check_collision.py $filename -s
done