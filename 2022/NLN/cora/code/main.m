function main()

    addpath(genpath('../code'));

    diary '../results/computationTime'
    fid = fopen('../results/results.csv','w');

    disp('Robertson ----------------------------------------------------------------------');

    text = example_nonlinear_reach_ARCH22_Robertson;
    fprintf(fid,'%s\n',text{1});
    fprintf(fid,'%s\n',text{2});
    fprintf(fid,'%s\n',text{3});
    saveas(gcf, '../results/Robertson.png');
    close(gcf);
    
    disp(' ')
    disp(' ')


    %disp('Van der Pol --------------------------------------------------------------------');
    %disp(' ');

    %text = example_nonlinear_reach_ARCH22_vanDerPol;
    %fprintf(fid,'%s\n',text);
    %saveas(gcf, '../results/vanDerPol.png');
    %close(gcf);
    
    %disp(' ')
    %disp(' ')
    
    
    disp('Laub Loomis --------------------------------------------------------------------');
    disp(' ');

    text = example_nonlinear_reach_ARCH22_laubLoomis;
    fprintf(fid,'%s\n',text{1});
    fprintf(fid,'%s\n',text{2});
    fprintf(fid,'%s\n',text{3});
    saveas(gcf, '../results/laubLoomis.png');
    close(gcf);
    
    disp(' ')
    disp(' ')


    disp('Lotka Volterra -----------------------------------------------------------------');
    disp(' ');

    text = example_hybrid_reach_ARCH22_lotkaVolterra;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/lotkaVolterra.png');
    close(gcf);
    
    disp(' ')
    disp(' ')
    
    
    disp('Spacecraft ---------------------------------------------------------------------');
    disp(' ');

    text = example_hybrid_reach_ARCH22_spacecraft;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/spacecraft.png');
    close(gcf);
    
    disp(' ')
    disp(' ')


    disp('Traffic Scenario ---------------------------------------------------------------');
    disp(' ');

    text = example_nonlinear_reach_ARCH22_trafficscenario;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/trafficscenario.png');
    close(gcf);

    disp(' ')
    disp(' ')
    
    % Close .csv file
    fclose(fid);
end