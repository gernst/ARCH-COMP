classdef strips
% strips - object constructor for strips
%
% Description:
%    Set defined by the intersection of multiple strips, where each strip
%    is defined as {x| |C(i,:)*x - y(i)| <= d(i)} 
%
% Syntax:  
%    obj = strips(C,d)
%    obj = strips(C,d,y)
%
% Inputs:
%    C - matrix storing the normal vectors of the strips
%    d - vector storing the strip bounds
%    y - vector storing the strip offset
%
% Outputs:
%    obj - generated strips object
%
% Example: 
%    C = [1 1; 1 -2];
%    d = [1; 0.5];
%    y = [1; 0];     
%    
%    S = strips(C,d,y);
%   
%    xlim([-5,5]); ylim([-5,5]);
%    plot(S,[1,2],'r');
%    xlim([-4,4]); ylim([-4,4]);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: interval, polytope

% Author:       Niklas Kochdumper
% Written:      21-November-2020 
% Last update:  21-March-2021 (MW, errConstructor)
% Last revision: ---

%------------- BEGIN CODE --------------

properties (SetAccess = protected, GetAccess = public)
    C (:,:) {mustBeNumeric} = [];   % matrix storing normal vectors
    d (:,1) {mustBeNumeric} = [];   % vector storing strip bounds
    y (:,1) {mustBeNumeric} = [];   % vector storing strip offset
    contSet = [];
end

methods

    % class constructor
    function obj = strips(C,d,varargin)
        
        % no argument is passed (default constructor)
        if nargin == 0
            obj.C = [];
            obj.d = [];
            obj.contSet = [];
        
        else
        
            obj.contSet = contSet(size(C,2));
            obj.C = C;
            obj.d = d;

            % check if C and d have matching dimensions
            if size(obj.C,1) ~= length(obj.d)
                [id,msg] = errConstructor('Input argument "d" has the wrong dimension');
                error(id,msg);
            end

            % parse third input argument
            if nargin < 3 || (nargin == 3 && isempty(varargin{1}))
                obj.y = zeros(size(obj.d));

            elseif nargin == 3
                % check if y has correct dimension
                if size(obj.C,1) ~= length(varargin{1})
                    [id,msg] = errConstructor('Input argument "y" has the wrong dimension');
                    error(id,msg);
                end
                obj.y = varargin{1};

            elseif nargin > 3
                % too many input arguments
                [id,msg] = errConstructor('Too many input arguments'); error(id,msg);
            end
        end
        
    end 
end

end