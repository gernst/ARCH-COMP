\section{Participants}
\label{sec:participants}

We briefly describe in alphabetical order all participating tools, the respective main ideas of the underlying approaches,
followed by details on how each tool was set up for the competition.

\subsection{\ARIsTEO}

\paragraph{Description.}
\ARIsTEO~\cite{ARIsTEO} is a Matlab toolbox
for test case generation against system specifications presented
in STL and it is developed on the top of \STaLiRo .  \ARIsTEO is
designed to targeting a large and practically-important category
of CPS models, known as \emph{compute-intensive} CPS (CI-CPS)
models, where a single simulation of the model may take hours to
complete.  \ARIsTEO embeds black-box testing into an iterative
approximation-refinement loop.  At the start, some sampled inputs
and outputs of the model under test are used to generate a
surrogate model that is faster to execute and can be subjected to
black-box testing.  Any failure-revealing test identified for the
surrogate model is checked on the original model.  If spurious,
the test results are used to refine the surrogate model to be
tested again.  Otherwise, the test reveals a valid failure.
\ARIsTEO is publicly available under the General Public License (GPL).%
\footnote{\url{https://github.com/SNTSVV/ARIsTEO}}

\paragraph{Setup.}
\ARIsTEO\ provides the same interface and parameters as \STaLiRo, while providing additional configuration options.
We had used an \textsc{arx} model (\textsc{arx}-$2$) with order $na=2$, $nb=2$, and $nk=2$\footnote{\url{https://nl.mathworks.com/help/ident/ref/arx.html}}
as structure for the surrogate model used in the approximation-refinement loop of \ARIsTEO .
For models with multiple inputs and outputs the dimension of the matrix $na$, $nb$ and $nk$ is changed depending on the number of inputs and outputs.
We used the default configuration of \STaLiRo\ for searching failure-revealing revealing tests on the surrogate model.
We considered the same parametrization of \STaLiRo\ for the input signals.
The original Simulink model was executed once to learn the initial surrogate model.  The cut-off values for the number of simulations of the original model and for the number of simulations of the surrogate model (per trial) were set to 300.
The results of \ARIsTEO\ can further improve by  (i)~using configurations for the surrogate model that provide more accurate approximations of the original models and more effectively guide the search toward faulty inputs; and (ii)~using the SOAR option of \STaLiRo that significantly improved the results of \STaLiRo compared with the last edition of this competition.



\subsection{\Breach}

\paragraph{Description.}
\Breach~\cite{Donze2010} is a Matlab toolbox
for test case generation, formal specification monitoring and
optimization-based falsification and mining of requirements for
hybrid dynamical systems. A particular emphasis is put on
modularity and flexibility of inputs generation, requirement
evaluation and optimization strategy. For this work, the approach
has been to ensure that each benchmark was properly implemented
and a default, relatively basic falsification strategy has been
applied. The idea was to perform a first systematic investigation
of the proposed problems, and then to provide a base to work on
for future editions of the competition to test a larger variety on
approaches on the most challenging instances.  Breach is available
under BSD license\footnote{\url{https://github.com/decyphir/breach}}.

\paragraph{Setup.} For most benchmarks (exceptions detailed below), a
piecewise constant signal generation was used with fixed step
size. For all instances, the optimization strategy used is the
default global Nelder Mead (GNM) approach with a custom
configuration for the competition, resulting in the following
three phases behavior:
\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
\item Phase 1.: at most $n_{\text{corners}}= $ 64 corner samples are tested, i.e., inputs
  for which control points take only extreme values;
\item Phase 2.: $n_{\text{quasi-rand}} = 100-n_{\text{corners}}$
  quasi-random samples from the Halton sequence with
  varying start points determined by a random seed are tested;
\item Phase 3.: the robustness results from phase 1 and 2 are
  sorted and Nelder Mead optimization is run from the most
  promising samples.
\end{itemize}
Note that as a result of this approach, whenever a falsifying
input is consistently found with less than 100 simulations, it
indicates that the problem is likely falsifiable with
extreme inputs or a quick stochastic exploration of the search
space.

For instance 2 in several problems, the pulse generation approach
investigated in \cite{ramezani2021} was used this year. This made it
possible to obtain slightly better results than previous editions
of the competitions. Specifically, CC4 could be falsified 3 times
out of 50 using two independent saturated pulse generators for the
2 inputs with varying periods, delays and width, and SCa could be
falsified 48 out of 50 times using a pulse generator with varying
period and delay.


\subsection{\FalCAuN}

\paragraph{Description.}
\FalCAuN{}~\cite{Waga20} is an experimental tool for testing a
Simulink model using black-box checking~\cite{PVY99}, an automated
testing method based on active automata learning and model
checking.  In \FalCAuN, the input and the output signals of the
Simulink model are discretized in time and values, and the model
is abstracted into a black-box Mealy machine.  \FalCAuN{} learns
the Mealy machine and conducts model checking to find a
counterexample.  \FalCAuN{} is designed to efficiently falsify a
Simulink model against multiple specifications by reusing the
learned Mealy machine.  \FalCAuN{} is publicly available under
General Public License (GPL)
v3\footnote{\url{https://github.com/MasWag/FalCAuN}}.

We utilize the \emph{discrete-time} semantics of STL, which is
essentially the same as the semantics of LTL.\@ Because of such
discretization, the control points must be fine enough to capture
the timing constraints in the STL formula.  For example, in order
to capture the timing constraint $\Diamond_{[0,0.05]}$, the
duration between the control points must be at most $0.05$.  We
note that due to the use of the discrete-time semantics, the
signal reported as a counterexample by \FalCAuN{} may not falsify
the model in terms of the continuous-time semantics.

\paragraph{Setup.}
For the signal discretization, we have the following hyperparameters: the (constant) duration of the intervals between samples, the input signal values at the control points, and the thresholds of the output signal values for the discretization.
%
We used the shortest duration between the control points such that the LTL encoding of the STL formula is small enough for the back-end model checker LTSMin. The duration ranges from 1.0 to 10.0 time units.
%
In most benchmarks, we let the input signal values be the maximum and the minimum of the range. The exceptions are as follows.
\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
 \item In AT6a, AT6b, and AT6c, the throttle can be 50 in addition to 0 and 100.
 \item In AT2 instance 2, the throttle and the brake can be 5 and 6 evenly spaced values, respectively, i.e., the value of the throttle can be one of 0, 25, 50, 75, and 100.
 \item In SCa, the input can be 4.00 in addition to 3.99 and 4.01.
\end{itemize}
%
In most benchmarks, we let the threshold of the output signal values be the thresholds in the STL formula. The exceptions are as follows.
\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
 \item In AT1 and AT2 instance 2, we have the common thresholds: 120 for the speed and 4750 for the RPM.\@
 \item In AT6a, AT6b, and AT6c, we have the common thresholds: 35, 50, and 65 for the speed and 3000 for the RPM.\@
 \item In CC1, CC2, and CC3, we have the common thresholds: 15 and 40 for $y_5 - y_4$ and 20 for $y_2 - y_1$.
\end{itemize}



\subsection{\falsify}

\paragraph{Description.}
\falsify is an experimental program which solves falsification
problems of safety properties by reinforcement
learning~\cite{yamagata2020falsification}.  \falsify uses a
\emph{grey-box} method, that is, it learns system behavior by
observing system outputs during simulation.  \falsify is currently
implemented by a deep reinforcement learning algorithm
\emph{Asynchronous Advantage Actor-Critic}
(A3C)~\cite{DBLP:conf/icml/MnihBMGLHSK16}.

\paragraph{Setup.}
The input specification uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
$\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.
The choice for the SC model was $\Delta T = 0.1$ model because Instance 2 uses $\Delta T = 1.75$, which is near to $\Delta T = 1$.


\subsection{\FalStar}

\paragraph{Description.}
\FalStar~\cite{ErnstSZH2018-FalStar} is an a falsification tool that explores the idea to construct falsifying inputs incrementally in time,
thereby exploiting potential time-causal dependencies in the problem.
The algorithm used in this competition is
called adaptive Las-Vegas tree search (aLVTS).
The main idea is to try ``simple'' inputs first, i.e., to scale gracefully with the intuitive combinatorial hardness of the benchmark problem~\cite{ernst2021tomacs}.
This is achieved by sampling from input domains with gradually finer temporal and spatial resolution, starting with extreme values.
The approach can be regarded as a more reasonable baseline over random sampling,
which takes the structure of the problem into account,
but which neither relies on sophisticated optimization methods
nor insight into the models themselves.
The code is publicly available under the BSD license.%
\footnote{\url{https://github.com/ERATOMMSD/falstar}}

\paragraph{Setup.} The search space for instance~1
included piecewise constant
inputs (the only parameterization currently supported), ranging
from~2 upto~4 control points at which discontinuities are allowed
(resp. upto~3 for NN).  In this configuration \FalStar benefits
from a low number of control points and is more likely to try
inputs with fewer control points first.  For the AT benchmarks it
was clear beforehand that this choice suffices to falsify all
benchmarks, and the setting was then kept for the remaining experiments.
Instance~2 input signals are parameterized with the number of control points as specified by the respective benchmarks.
The F16 benchmark does not have a time-varying input
and therefore the strategy proposed in~\cite{ErnstSZH2018-FalStar}
is not applicable. Instead, global optimization with Nelder/Mead is used
on this benchmark, which is effective.

\subsection{\foresee}

\paragraph{Description.}
In falsification, the \emph{scale problem}
can occur when the signals used in the specification have
different scales (e.g., rpm and speed): namely, the contribution
of a signal could be \emph{masked} by another one when computing
robustness. \foresee~\cite{falsQBRobCAV2021} (\texttt{FOR}mula
\texttt{E}xploitation by \texttt{S}equence tr\texttt{EE}) tackles
this problem by introducing a new robustness definition, called
{\it \QBRobName}, which combines quantitative robustness and
classical Boolean satisfaction. \QBRobName does not require
comparing (i.e., by minimum or maximum) robustness values of
different sub-formulas, so possibly avoiding the scale
problem. However, in order to be computed, \QBRobName requires the
selection of a sequence of sub-formulas along the syntax tree of
the specification for which to compute the quantitative
robustness. Different sub-formulas sequences can be more or less
effective in mitigating the scale problem.

\foresee implements a
falsification strategy based on a Monte Carlo Tree Search over the
structure of the formal specification: first, by tree traversal,
it identifies the sub-formulas sequence; then, on the leaves, it
performs numerical hill-climbing optimization, with the aim of
falsifying the selected sub-formulas. \foresee is the spiritual successor of
\FalStar/MCTS from \cite{ernst2019arch,ernst2020arch}.
It is publicly available under GNU General Public License (GPL)
v3.\footnote{\url{https://github.com/choshina/ForeSee}}

\paragraph{Setup.}
Since \foresee is implemented on the basis of \Breach, it provides the same interface of \Breach, namely, users can characterize the shape of input signals with a number of options, including piecewise constant, piecewise linear, pulse, etc. In this report, we regulate the shape of input signals with piecewise constant, parametrized by the number of \emph{control points}. 

In the current implementation of \foresee, only CMA-ES~\cite{AugerH05} is provided as the optimizer; this is due to our insight in the performances of different optimizers, in which CMA-ES outperforms other optimizers. However, involving other optimizers is not difficult for \foresee, and will be considered in the future releases.

Since  \foresee technically relies on Monte Carlo Tree Search (MCTS), the hyperparameters in MCTS need to be properly selected. As a default setting, we use 0.2 as the scalar in the UCB1 algorithm, that takes a balance of \emph{exploration} and \emph{exploitation}; and we set 10 generations as the budget for the playout phase of MCTS.


\subsection{\STaLiRo}

\paragraph{Description.}
\STaLiRo~\cite{annpureddy2011s} is a Matlab
toolbox for monitoring and test case generation against system
specifications presented in STL (or MTL). The test cases are automatically
generated using optimization techniques guided by formal
requirements in STL in order to find falsifying systems behaviors.
The tool supports different optimization algorithms. 
In past competitions, the Stochastic Optimization with Adaptive Restarts (SOAR) \cite{mathesen2019soar} framework was used for all the benchmarks except for choosing instance 1 type inputs in Steam Condenser model. 
In that benchmark, Simulated annealing global search was combined with a local optimal control based search \cite{YaghoubiHSCC}.  
For the 2021 competition, we have used the minSOAR framework \cite{MathesenPF2021case,Mathesen2021dissertation} for the newly proposed conjunctive benchmarks.
The minSOAR framework can be thought of as an extension to SOAR which can handle multiple conjunctive requirements by modeling information from each resulting robustness component.
\STaLiRo is publicly available on-line under General Public License (GPL) \footnote{\url{
    https://sites.google.com/a/asu.edu/s-taliro}}.

\paragraph{Setup.}
In S-TaLiRo, input signals are parameterized in two ways:
the number of control points for the input signal, and the
time location of those control points during simulation. The number of
control points for each input signal is given by the user forming
an optimization problem with search space dimension the same as the number of
control points. An option is provided to the user to add to the search space
the timing of the control points, but this option is not used in the competition.
For this competition, the control point time locations are evenly spaced over the duration
of the simulation for all the benchmarks except for the SC problem instance 1.

For the transmission model the $[\mathit{throttle}, \mathit{brake}]$ control points
are interpolated with the \textit{pchip} function, with $[7, 3]$ as
the number of control points in specifications 1-6 and $[4,2]$ for
7-9 to reduce the dimensionality of the search space. For the Neural model, we use $13$
control points to yield piecewise constant signals of $3.33$ seconds apart.
%The Chasing Cars model used $5$ control points for piecewise constant signals of $20$ seconds each.
The Wind Turbine used the default model input of $126$ control points interpolated linearly.
For the SC model, Simulated Annealing (SA) global search was utilized in combination with an optimal control based local search on the infinite dimensional input space. The SA global search utilizes piecewise constant inputs with 12 possibly uneven time durations.



\subsection{\zlscheck}

\paragraph{Description.}
\zlscheck is a tool for test case generation of programs written in Zélus\footnote{\url{http://zelus.di.ens.fr/}}~\cite{zelus}, a language reminiscent of the synchronous languages Lustre~\cite{lustre} and Scade~\cite{scade} extended in order to express ODEs. For now \zlscheck applies to the discrete-time subset of Zélus. \\
Properties are expressed as synchronous observers~\cite{sync_obs}
with a quantitative semantics to solve the falsification problem
as an optimization problem.  \zlscheck uses automatic
differentiation to compute gradients of the robustness of a model
w.r.t. some input parameters and uses gradient-based techniques to
find a falsifying input.
Additionally, it uses FADBADml\footnote{\url{https://fadbadml-dev.github.io/FADBADml/}}, a tool for automatic differentiation which we ported from C++ to OCaml. \\
All the models of this competition have been rewritten manually in Zélus and are available along with the tool on github\footnote{\url{https://github.com/ismailbennani/zlscheck}}. The Simulink's Integrator block is programmed in Zélus as a fixed-step forward euler scheme. \\
The counter-examples found by \zlscheck were systematically
validated on their corresponding Simulink models, the ones that
did not pass validation were discarded from the final results.%

\paragraph{Setup.}
The inputs of the systems are bounded piecewise constant streams. A bounded piecewise constant stream $x$ of size $N$ and period $k$ is such that $\forall n \in [0,N], x(n) = x(\lfloor\frac{n}{k}\rfloor\cdot k)$. It is totally defined by $(\lfloor \frac{N}{k} \rfloor + 1)$ values (parameters). \\
Two different strategies were used in these benchmarks:
\begin{itemize}
    \item \underline{classic}: the optimization is done offline: the parameters are generated at the beginning of the simulation, then the corresponding robustness is computed. The optimization algorithm then uses the robustness and its gradient w.r.t. the parameters to compute the next parameters. \\
    This strategy has been used for properties AT1, AT2, AT6, AFC29, AFC33, NN, WT, F16 and SC.

    \item \underline{mode switch}: the optimization is performed online: for each input, the parameter number $\lfloor\frac{n}{k}\rfloor$ is generated at step $\lfloor\frac{n}{k}\rfloor\cdot k$ of the simulation. \\
    In order to achieve coverage of the different modes of the system (a mode is a state in a hierarchical automaton), \zlscheck chooses randomly an available transition and drives the system towards triggering it: it computes a quantitative interpretation of the complement of its guard (transition robustness) and the gradients of that robustness w.r.t. the current values of the inputs of the system (at step $n$ this would be the parameters number $\lfloor\frac{n}{k}\rfloor$). The value of the next parameter is then computed by the optimization algorithm using its precedent value and the gradient. \\
    Once the transition has been triggered, the tool chooses a new one randomly and so on, until the simulation ends. In this mode, the inputs are generated independently of the property being falsified. \\
    This strategy has been used for properties AT5, AFC27 and CC.
\end{itemize}
The gradient-based algorithm used by \zlscheck is a simple gradient descent with decreasing step-size: at step $l$ of the optimization, the step-size is $a(l) = a(0) / \sqrt l$ where $a(0)$ is a metaparameter. The first input is sampled uniformly in the input space, and if the same input is generated twice in a row, the algorithm restarts.
Other gradient descent algorithms (ADAM, ADAGRAD, AMSGRAD) are implemented in \zlscheck and have been tested but did not give significantly better results. \\
Additionally, given that the integration scheme is fixed-step, we can express the period $k$ of the inputs as times instead of number of simulation steps. For instance 1, those periods are (in seconds):
${\Delta T_{AT} = 0.5}$ except ${\Delta T_{AT2} = 2.5}$, ${\Delta T_{AT6a} = 5}$, ${\Delta T_{AT6b} = 10}$, ${\Delta T_{AT6c} = 15}$ and
${\Delta T_{AFC} = 5}$ and ${\Delta T_{NN} = 3}$ and ${\Delta T_{WT} = 5}$ and ${\Delta T_{CC} = 5}$ and ${\Delta T_{F16} = +\infty}$, ${\Delta T_{SC} = 0.2}$.



