function [R,Rout] = observe_backward(obj,options)
% observe_backward - computes the guaranted state estimation approach
%                    using backwards propagation of strips
%
% Syntax:  
%    [R,Rout] = observe_backward(obj,options)
%
% Inputs:
%    obj - discrete-time linear system object
%    options - options for the guaranteed state estimation
%
% Outputs:
%    R - observed set of points in time
%    Rout - observed set of measurements of points in time 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Niklas Kochdumper
% Written:       23-November-2020
% Last update:   ---
% Last revision: ---


%------------- BEGIN CODE --------------

    % intitialization
    R = cell(size(options.yVec,2),1);
    R{1} = options.R0;

    c = obj.c;
    if isempty(c)
       c = zeros(size(obj.A,1),1); 
    end
    D = obj.D;
    if isempty(D)
       D = 0; 
    end
    k = obj.k;
    if isempty(k)
       k = zeros(size(obj.C,1),1); 
    end
    
    % precompute inverse of system matrix
    Ainv = inv(obj.A);

    % loop over all measurements
    for i = 2:size(options.yVec,2)
       
        % construct strip from current measurement
        y = options.yVec(:,i);
        int = interval(y-options.sigma,y+options.sigma) - ...
              interval(D*options.U + options.V) - k;
        
        S = strips(obj.C,rad(int),center(int));
        
        % backpropagate strip one time step
        S_ = Ainv*S + (-Ainv)*obj.B*options.U + (-Ainv)*options.W + ...
                                                                Ainv*(-c);
        
        % intersect strip with reachable set
        X = and(S_,R{i-1},'svd');
        
        % propagate reachable set forward in time
        X = obj.A * X + obj.B * options.U + options.W + c;
        
        % order reduction
        if isa(X,'zonotope')
            X = reduce(X,'girard',options.zonotopeOrder);
        end    
            
        % intersect strip with reachable set
        R{i} = and(S,X,'svd');
    end
    
    Rout = R;

%------------- END OF CODE --------------