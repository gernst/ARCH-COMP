#!/bin/bash 
function run {
	echo $2 ":"
	spaceex -g $1$2.cfg -m $1$3.xml -vl | grep 'Forbidden\|Computing reachable states done after'
}
function graphplot {
	echo $2 ":"
	spaceex -g $1$2.cfg -m $1$3.xml -o $1$2.gen -vl | grep 'Forbidden\|Computing reachable states done after'
	graph -Tpng --bitmap-size 1024x1024 -C -B -q0.5 $1$2.gen > $1$2.png
}

run AdaptiveCruiseController/ ACCS05-UBD05 ACCS05-UBD05
run AdaptiveCruiseController/ ACCU05-UBD05 ACCU05-UBD05
run AdaptiveCruiseController/ ACCS06-UBD06 ACCS06-UBD06
run AdaptiveCruiseController/ ACCU06-UBD06 ACCU06-UBD06
run DistributedController/ DISC02-UBS02 DISC02-UBS02
run DutchRailwayNetwork/ DRNW01-BDS01 DRNW01
run DutchRailwayNetwork/ DRNW01-BDU01 DRNW01
run DutchRailwayNetwork/ DRNW02-BDR01 DRNW02
run DutchRailwayNetwork/ DRNW02-BRS01 DRNW02
run Fischer/ FISCS04-UBD04 FISCS04-UBD04
run Fischer/ FISCU04-UBD04 FISCU04-UBD04
run TTEthernet/ TTES05-UBD05 TTES05-UBD05
run TTEthernet/ TTES07-UBD07 TTES07-UBD07
