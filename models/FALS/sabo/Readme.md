# Security Benchmarks for ARCH 2023

## Stealthy attacks formalized as STL formulas for Falsification of CPS Security

We propose a framework for security vulnerability analysis for Cyber- Physical Systems (CPS). Our framework imposes only minimal assumptions on the structure of the CPS. Namely, we consider CPS with feedback control loops, state observers, and anomaly detection algorithms. Moreover, our framework does not require any knowledge about the dynamics or the algorithms used in the CPS. Under this common CPS architecture, we develop tools that can identify vulnerabilities in the system and their impact on the functionality of the CPS. We pose the CPS security problem as a falsification (or Search Based Test Generation (SBTG)) problem guided by security requirements expressed in Signal Temporal Logic (STL). We propose two different categories of security requirements encoded in STL: (1) detectability (stealthiness) and (2) effectiveness (impact on the CPS function). Finally, we demonstrate in simulation on an inverted pendulum and on an Unmanned Aerial Vehicle (UAV) that both specifications are falsifiable using our SBTG techniques.

The paper is titled set `Stealthy attacks formalized as STL formulas for Falsification of CPS Security` and is set to appear at HSCC'23.

### Authors and Affiliation:

ANIRUDDH CHANDRATRE, TOMAS ACOSTA, TANMAY KHANDAIT, and GIULIA PEDRIELLI, School of Computing and Augmented Intelligence, Arizona State University, USA

GEORGIOS FAINEKOS, Toyota NA R&D, USA

## What to expect from the code.

In this repository, you will find two demo files that demonstrates the usage of the UAV and Pendulum model using dummy signals, and also compute their robustness using RTAMT.

The benchmarks are available at https://gitlab.com/sbtg/security/sabo/-/tree/benchmarks.

To ensure that this repository acts as a guide, I have provided installation files using a dependency management tool called Poetry.

## Installation

We use the Poetry tool which is a dependency management and packaging tool in Python. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you. Please follow the installation of poetry at https://python-poetry.org/docs/#installation

After you've installed poetry, you can install all the dependencies by running the following command in the root of the project:

```
poetry install
```

## A. Running the models

### 1. Pendulum Model

Let us walk through the steps for running the Pendulum model.

1. Define STL Parameters

   ```
    stl_params = {
            'model': 'pendulum',
            'eta': 0.223,
            'gamma': 0.9,
            'epsilon': 0.1,
            'alpha': 0.028
        }
   ```
2. Generate Input Signals

   The following code generates the time signal of 15 seconds, with a step value of 0.01. The `input_signals` matrix consists of `2 signals`, which is provided to the the model.

   ```
   import numpy as np
   
   step = 0.01
   times = np.arange(0,15+step, 0.01)
   input_signals = np.random.rand(len(times),2).T
   ```
3. Run the model

   ```
   timestamps, trajectories = pendulum_model(
                       static = [], 
                       times = times,
                       signals=input_signals, 
                       params = stl_params
                   )
   ```

### 2. UAV Model

Let us walk through the steps for running the UAV model.

1. Define STL Parameters

   ```
   stl_params = {
            'model': 'uav',
            'sim_end_time': 25,
            'eta': 0.223,
            'gamma': 0.9,
            'epsilon': 0.1,
            'alpha': 0.028
        }
   ```
2. Generate Input Signals

   The following code generates the time signal of 25 seconds, with a step value of 0.01. The `input_signals` matrix consists of `6 signals`, which is provided to the the model.

   ```
   import numpy as np
   
   step = 0.01
   times = np.arange(0,25+step, 0.01)
   input_signals = np.random.rand(len(times),6).T
   ```
3. Run the model

   ```
   timestamps, trajectories = uav_model(
                       static = [], 
                       times = times, 
                       signals = signals, 
                       params = stl_params
                   )
   ```

## Defining Specifications

We have two specifications which can be found in our paper in at https://gitlab.com/sbtg/security/sabo-hscc23-rep/-/tree/main/

The output contains 2 signals: (1) $`P(g_t>\eta)`$ ; and (2) $`P(||x_t - \hat x_t)|| > \alpha`$ .

1. Instantaneous Attack Specification $`abc=1`$

   The first specification family requests for the following:

   ```
   Producing stealthy attacks with respect to the anomaly detector while the system state error should eventually cross a vertain threshold.
   ```

   Note that this is not a safety requirement, but rather a temporally extended goal for the attacker to achieve the goal. Therefore, a safety requirement for falsification should be the negation of the goal.

   The STL formula is given by: 

```math
\neg ( \Box_{[0, T]} [P(g_t > \eta) \leq \epsilon] \wedge \Diamond_{[0, T]} [P (||x_t - \hat{x}\_t|| \geq \alpha) \geq \gamma])
```
   $`T`$ is the total simulation time, $`g_t`$ is the anomaly as a function of time, $`\eta`$ is the anomaly detection threshold beyond which the system triggers an alarm. The parameter $`\epsilon`$ is the probability bound for the stealthiness of the attack, while $`||x_t - \hat{x}_t||`$ is the norm of the system state error, $`\alpha`$ is the lower bound of the desired error, and $`\gamma`$ is the probability bound for the effectiveness of the attack.
2. $`\delta`$-Sustained Attach Specification

   The second specification family models attacks which should not trigger an alarm until the end of a sustained attack of $`\delta`$ duration.

   The corresponding STL formula for falsification (as a negation of the desired goal) is: 
```math
\neg ([P(g_t > \eta) \leq \epsilon] \text{U}_{[0, T]} \boxminus_{[0, \delta]} [P (||x_t - \hat{x}_t|| \geq \alpha) \geq \gamma])
```
   where $`T, g_t, ||x_t - \hat{x}_t||, \eta, \epsilon, \alpha, \gamma`$ follow the same definition as for the instantaneous attack. For the experiments, we set the value of $`\delta = 0.06`$, since from pilot simulation, it resulted sufficient to find violations (exploits).

   ## Side Note:

   This work treats probabilities as signals. That is, instead of using a probabilistic logic, we compute probabilities, e.g. $`P(g_t > \eta)`$, over time and we write requirements on the probability thresholds, e.g. $`P(g_t > \eta)<\epsilon`$. This way, we can use the standard definitions of STL robustness as a heuristic cost function for falsification.

   To compute the probabilities, since the system being attacked is stochastic, we collect $`n`$ trajectories produced from an attack signal under $`n`$ different random seeds. Probabilities over these $`n`$ different signals are computed at every time step and are given in the form of a signal that enables the STL robustness computation for Instantaneous and $`\delta`$-Sustained attacks. The probabilities are also used for an attack signal's evaluation (SSR, AE, SR).

## Running the Examples scripts

```
poetry run python test_pendulum.py
poetry run python test_uav.py
```
